# Randsomewhere

Multithreaded AES-GCM encryption for your files.
This is a quick proof of concept...

## Usage

Help :
```
randsomewhere -h
```

Encrypt /home/toto/* :
```
 ./randsomware -p "/home/toto/" --encrypt
```

Decrypt /home/toto/*
```
 /randsomware -p "/home/toto/" --decrypt --key=PREVIOUSLY_GENERATED_KEY
```

Note: do not use relative paths
